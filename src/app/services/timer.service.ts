import { Injectable } from '@angular/core';
import { BehaviorSubject, interval, share } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  private _timer$ = new BehaviorSubject<number | null>(null);

  public timer$ = this._timer$.asObservable();

  private interval = interval(1000)
    .subscribe(this._timer$)
}
