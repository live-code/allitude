import { inject, Injectable } from '@angular/core';
import { BehaviorSubject, map, withLatestFrom } from 'rxjs';
import { AuthService } from './auth.service';
/*
export interface Theme {
  value: 'dark' | 'light'
}*/

export type Theme = 'dark' | 'light';
export type Language = 'it' | 'en';

export type Config = {
  language: Language,
  theme: Theme
}

const initialState: Config = {
  language: 'it',
  theme: 'light'
}


@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private _config$ = new BehaviorSubject<Config>(initialState)
  config$ = this._config$.asObservable()
  authService = inject(AuthService);

  setConfig(cfg: Partial<Config>) {
    this._config$.next({
      ...this._config$.getValue(),
      ...cfg
    });
  }

  setTheme(theme: Theme) {
    this._config$.next({ ...this._config$.getValue(), theme })
  }

  setLanguage(language: Language) {
    this._config$.next({ ...this._config$.getValue(), language })
  }

  get theme() {
    return this._config$.getValue().theme
  }


  get theme$() {
    return this._config$
      .pipe(
        map(cfg => cfg.theme)
      )
  }

  get languageHumanFriendly$() {
    return this._config$
      .pipe(
        map(cfg => cfg.language === 'it' ? 'italiano' : 'inglese')
      )
  }

  get configWithAuth$() {
    return this._config$
      .pipe(
        withLatestFrom(this.authService.token$),
        map(([config, token]) => ({ config, token }))
      )
  }

  /*
  constructor() {
    const theme = localStorage.getItem('theme') as Theme
    if (theme) {
      this._theme.next(theme)
    }
  }*/


}
