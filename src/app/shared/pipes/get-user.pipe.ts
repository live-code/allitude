import { HttpClient } from '@angular/common/http';
import { inject, Pipe, PipeTransform } from '@angular/core';
import { map } from 'rxjs';
import { User } from '../../model/user';

@Pipe({
  name: 'getUser',
  standalone: true
})
export class GetUserPipe implements PipeTransform {
  http = inject(HttpClient);

  transform(id: number): any{
    return this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + id)
      .pipe(
        map(res => res.name)
      )
  }

}
