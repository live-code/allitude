import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getTitle',
  standalone: true,
})
export class GetTitlePipe implements PipeTransform {
  transform(role: 1 | 2 | 3, isUppercase: boolean = false) {
    let text = '';
    switch(role) {
      case 1:
        text = 'guest';
        break;
      case 2:
        text = 'admin';
        break;
      case 3:
        text = 'moderator';
        break;
    }

    return isUppercase ?
      text.toUpperCase() : text
  }

}
