import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';
import { ConfigService } from '../services/config.service';

@Component({
  selector: 'app-nav-bar',
  standalone: true,
  imports: [CommonModule, RouterLink],
  template: `
    <!--<div
      class="navbar"
      [ngClass]="{
        'dark': (configService.theme | async) === 'dark',
        'light': (configService.theme | async) === 'light'
      }"
    >-->
      <button routerLink="demo1">demo1</button>
      <button routerLink="demo2">demo2</button>
      <button routerLink="demo3">demo3</button>
      <button routerLink="demo4">demo4</button>
      <button routerLink="demo5">demo5</button>
      <button routerLink="demo6">demo6</button>
      <button routerLink="demo7">demo7</button>
      <button routerLink="demo8">demo8</button>
      <hr>
    <!--</div>-->
  `,
  styles: [`
    .navbar {
      padding: 10px;
    }
    .dark {
      background-color: #000;
      color: white;
    }
    .light {
      background-color: #ccc;
      color: black;
    }
  `]
})
export class NavbarComponent {
  configService = inject(ConfigService);
}

