import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { delay, share } from 'rxjs';
import { User } from '../model/user';
import { TimerService } from '../services/timer.service';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [CommonModule],
  template: `
    <li *ngFor="let u of users$ | async ">{{u.name}}</li>
    <pre>total: {{(users$ | async)?.length}}</pre>
    
    <button (click)="getUsers()">GET USERS</button>
  `,
})
export default class Demo5Component {
  timerService = inject(TimerService)

  users$  = inject(HttpClient)
    .get<User[]>('https://jsonplaceholder.typicode.com/users')
    .pipe(
      share(),
      delay(2000),
    )

  constructor() {

    this.timerService.timer$
      .pipe(
        takeUntilDestroyed()
      )
      .subscribe(
        val => console.log('demo5', val)
      )

    this.users$
      .subscribe({
        next: (res) => console.log(res),
        error: () => console.log('ahia1')
      })
  }

  getUsers() {
    this.users$
      .subscribe({
        next: (res) => console.log(res),
        error: () => console.log('ahia2')
      })
  }
}
