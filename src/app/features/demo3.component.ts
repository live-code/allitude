import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, inject, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { concatMap, delay, exhaustMap, fromEvent, map, mergeAll, mergeMap, of, switchMap, tap } from 'rxjs';
import { Post } from '../model/post';
import { User } from '../model/user';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [CommonModule, RouterLink],
  template: `
    <h1>Nested Calls</h1>
    <button #btn>CLICK ME</button>
    
    <button routerLink="/demo3/88">88</button>
    <button routerLink="/demo3/99">99</button>
    <pre> {{data?.post | json}}</pre>
    <pre> {{data?.user | json}}</pre>
  `,
})
export default class Demo3Component  {
  @ViewChild('btn') button!: ElementRef<HTMLButtonElement>
  http = inject(HttpClient)
  activateRoute = inject(ActivatedRoute)

  getPost$ = (id: number) => this.http.get<Post>('https://jsonplaceholder.typicode.com/posts/' + id)
  getUser$ = (post: Post) => this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + post.userId)
    .pipe(
      map(user => ({ user, post }))
    )


  data: { user: User, post: Post } | undefined;

  constructor() {
    this.activateRoute.params
      .pipe(
        mergeMap((params) => this.getPost$(params['postId'])),
        mergeMap((post) => this.getUser$(post)),
      )
      .subscribe(console.log)
  }

}
