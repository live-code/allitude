import { Component, EventEmitter, inject, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, map, Subject, withLatestFrom } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { ConfigService, Theme } from '../services/config.service';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [CommonModule],
  template: `
    <h1>
      State Behavior Subject example
      {{(configService.configWithAuth$ | async)?.config}}
      {{(configService.configWithAuth$ | async)?.token}}
    </h1>
    <p>
      Current theme:
      {{configService.config$ | async | json}}
      {{(configService.config$ | async)?.theme}}
    </p>

    <button
      (click)="configService.setConfig({ theme: 'light' })"
    >Change to Light
    </button>
    <button
      (click)="configService.setTheme('dark')"
    >Change to Dark
    </button>

    <button
      (click)="configService.setConfig({ language: 'it'})"
    >Change to IT
    </button>
    <button
      (click)="configService.setLanguage('en')"
    >Change to EN
    </button>

    <button
      (click)="configService.setConfig({ theme: 'dark', language: 'it'})"
    >Change to Dark
    </button>
  `,
})
export default class Demo6Component {
  configService = inject(ConfigService);
/*
  constructor() {
    this.configService.configWithAuth$
      .subscribe((data) => {
        console.log(data.config, data.token)
      })
  }*/
}
