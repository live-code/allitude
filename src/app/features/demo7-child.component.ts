import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-demo7-child',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      demo7-child works!
    </p>
    
    <div *ngIf="visibility">SHOW CONTENT</div>
  `,
})
export class Demo7ChildComponent {
  @Input() visibility: boolean = false;

}
