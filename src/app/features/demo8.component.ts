import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { User } from '../model/user';
import { GetUserPipe } from '../shared/pipes/get-user.pipe';

@Component({
  selector: 'app-demo8',
  standalone: true,
  imports: [CommonModule, GetUserPipe],
  template: `
    <p>
      demo8 works!
    </p>

    <h1>{{userId | getUser | async}}</h1>
    
    <button (click)="userId = 1">1</button>
    <button (click)="userId = 2">2</button>
    <button (click)="userId = 3">3</button>
  `,
})
export default  class Demo8Component {
  userId = 2;
}
