import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { catchError, debounceTime, distinctUntilChanged, filter, forkJoin, mergeMap, of, switchMap, tap } from 'rxjs';
import { Post } from '../model/post';
import { User } from '../model/user';
import { TimerService } from '../services/timer.service';

@Component({
  selector: 'app-demo4',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  template: `
    <p>
      demo4 works!
    </p>
    
    <h1 *ngIf="meteo$ | async as met">
      {{met.main.temp}}
    </h1>
    
    <input type="text" [formControl]="input">
    
  `,
})
export default class Demo4Component {
  http = inject(HttpClient)
  timerService = inject(TimerService)

  input = new FormControl();
  meteo$ = this.input.valueChanges
      .pipe(
        debounceTime(1000),
        distinctUntilChanged(),
        filter(text => text.length > 2),
        switchMap(text => this.getMeteo$(text))
      );


  getMeteo$ = (city: string) => this.http.get<any>(`http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
    .pipe(
      catchError(err => {
        return of(null)
      })
    )

  constructor() {
    this.timerService.timer$
      .pipe(
        takeUntilDestroyed()
      )
      .subscribe(val => console.log('demo4', val))

    return;
/*    // --------xo---->
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/pXhotos')
      .pipe(
        catchError(() => {
          return of(123)
        })
      )
      .subscribe({
        next: (res) => console.log('ok', res),
        error: () => console.log('ahia!'),
        complete: () => console.log('completed')
      })*/


    const users$ = this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .pipe(
        catchError(() => {
          return of([]);
        })
      )

    const posts$ = this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts')
      .pipe(
        catchError(() => {
          return of(null);
        })
      )

    forkJoin({ users: users$, posts: posts$ })
      .subscribe({
        next: (res) => console.log('ok', res),
        error: () => console.log('ahia!'),
        complete: () => console.log('completed')
      })
  }
}
