import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { combineLatest, forkJoin, fromEvent, interval, Subject, Subscription, take, takeUntil, takeWhile } from 'rxjs';
import { Post } from '../model/post';
import { User } from '../model/user';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  template: `
    <p>
      demo2 works!
    </p>
    
    <input [formControl]="input" />
    <select [formControl]="select">
      <option value="1">1</option>
      <option value="2">2</option>
    </select>
    
  `,
})
export default class Demo2Component implements OnInit {
  http = inject(HttpClient)
  input = new FormControl()
  select = new FormControl()
  timer$ = interval(1000).pipe(takeUntilDestroyed())

  form = combineLatest({
    text: this.input.valueChanges,
    select: this.select.valueChanges
  })

  reqs = forkJoin({
    users: this.http.get<User[]>('https://jsonplaceholder.typicode.com/users'),
    posts: this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts'),
  })

  combine = combineLatest({ form: this.form, reqs: this.reqs })
    .pipe(takeUntilDestroyed())

  ngOnInit() {
    this.timer$.subscribe(console.log)
    this.combine
      .subscribe(res => {
        console.log(res)
      })
  }

}
