import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { interval, timer } from 'rxjs';
import { User } from '../model/user';

@Component({
  selector: 'app-demo1',

  standalone: true,
  imports: [CommonModule],
  template: `
      demo1 works!
      <pre>{{users$ | async | json}}</pre>
      
      <li *ngFor="let u of users$ | async">
        {{u.name}}
      </li>
  `,
})
export default class Demo1Component {
  users$  = inject(HttpClient)
    .get<User[]>('https://jsonplaceholder.typicode.com/users')

  timer$ = interval(1000)
}



