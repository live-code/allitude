import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { GetTitlePipe } from '../shared/pipes/get-title.pipe';
import { Demo7ChildComponent } from './demo7-child.component';
import vi = CSS.vi;


@Component({
  selector: 'app-demo7',
  standalone: true,
  imports: [CommonModule, FormsModule, Demo7ChildComponent, GetTitlePipe],
  template: `
    <p *ngIf="visible">msg</p>

    <h1>Title {{layout | getTitle: true}}</h1>
    <h1>Title {{layout | getTitle}}</h1>
    
    <app-demo7-child [visibility]="visible"  />


    <div *ngIf="name.length > 3">Lungo</div>
    <input type="text" [(ngModel)]="name">
    <button (click)="visible = !visible">Toggle</button>

    <button (click)="layout = 1">1</button>
    <button (click)="layout = 2">2</button>
    <button (click)="layout = 3">3</button>

    {{render()}}

  `,
})
export default class Demo7Component {
  name: string  = '';
  visible = false;
  layout: 1 | 2 | 3 = 1;

  doSomething() {
    this.visible = !this.visible
  }

  getTitle() {
    console.log('title render')

  }

  render() {
    console.log('render parent')
  }

  protected readonly vi = vi;
}
